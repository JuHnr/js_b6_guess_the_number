window.addEventListener('DOMContentLoaded', function () {

    let randomNumber; //nombre à deviner
    let tries; //nombre de tentatives de l'utilisateur
    const result = document.querySelector(".result"); //récupère l'élément de classe .result dans le DOM
    const input = document.getElementById("guess"); //récupère l'input d'ID #guess dans le DOM
    const recordResult = document.querySelector(".record_result");

    function startGame() {
        tries = 1;
        randomNumber = Math.floor(Math.random() * 100); //génère un nombre aléatoire entier entre 0 et 100
        console.log('Le nombre à deviner est : ' + randomNumber);
    }

    //appel de la fonction
    startGame();

    //récupère l'élément d'id "guess_form"
    const form = document.getElementById("guess_form");

    //écoute de l'évènement submit sur form. A la soumission du formulaire (clic sur bouton submit), exécute la fonction submitForm.
    form.addEventListener('submit', submitForm);

    function submitForm(event) {
        event.preventDefault();  //empêche le comportement par défaut (qui est de rafraîchir la page),
        console.log('soumission du formulaire'); //confirme que le formulaire a bien été soumis

        //récupère la valeur de "guess" => fonctionnalité des formulaires : accès direct à un élément du form grâce au name (ici l'input de l'utilisateur)
        const value = form.guess.value;

        //affiche value dans la console
        console.log(value);

        //Réinitialise les classes de result pour appliquer le bon style
        function clearClass() {
            result.classList.remove("alert", "bigger", "smaller", "success");
        }

        //supprime la valeur entrée par l'utilisateur
        function clearInput() {
            input.value = "";
        }


        if (value < 0 || value > 99) {
            clearClass(); //appel de la fonction pour réinitialiser les classes de result
            result.classList.add("alert"); //ajoute la classe .alert
            result.textContent = "Number to guess is between 0 and 99. Try another number."; //affiche le résultat dans le DOM
            clearInput(); //appel de la fonction pour supprimer la proposition de l'utilisateur

        } else if (value < randomNumber) {
            clearClass();
            result.classList.add("bigger");
            result.textContent = "Number to guess is bigger";
            clearInput();
            tries++; //le nombre d'essai est incrémenté

        } else if (value > randomNumber) {
            clearClass();
            result.classList.add("smaller");
            result.textContent = "Number to guess is smaller";
            clearInput();
            tries++;

        } else if (value == randomNumber) {
            clearClass(); 
            result.classList.add("success");
            result.textContent = `Congratulations ! The number to guess was ${randomNumber}.`;

            /*LocalStorage*/

            //vérifie si localStorage est disponible
            if (storageAvailable("localStorage")) {

                if (isARecordStored()) { //vérifie si un record est déjà rentré

                    getRecordStored(); //récupère la valeur du record 

                    if (isNewRecord()) { //vérifie si c'est un nouveau record
                        recordStorage(tries); //stocke la valeur de tries
                        recordResult.textContent = `New record ! (${tries} tries)`;
                        
                    } else {
                        recordResult.textContent = `You guessed it in ${tries} tries. You didn't do a new record. Try again !`;
                    }

                } else { // cas où il n'y a pas encore de record enregistré
                    recordStorage(tries); //stocke la valeur de tries
                    recordResult.textContent = `New record ! (${tries} tries)`;
                }

            } else { //cas où localStorage n'est pas disponible sur le navigateur
                recordResult.textContent ="localStorage isn't available";
            }


            clearInput(); //réinitialise l'input
            startGame(); //réinitialisation du jeu pour recommencer une partie

        }

    }

    /*fonctions utilisées pour localStorage*/

    //vérifie la disponibilité de localStorage
    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = "__storage_test__";
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        } catch (e) {
            return (
                e instanceof DOMException &&
                // everything except Firefox
                (e.code === 22 ||
                    // Firefox
                    e.code === 1014 ||
                    // test name field too, because code might not be present
                    // everything except Firefox
                    e.name === "QuotaExceededError" ||
                    // Firefox
                    e.name === "NS_ERROR_DOM_QUOTA_REACHED") &&
                // acknowledge QuotaExceededError only if there's something already stored
                storage.length !== 0
            );
        }
    }

    //enregistre une valeur dans le stockage
    function recordStorage(totalTries) {
        localStorage.setItem("record", totalTries);
    }

    //vérifie qu'il y a déjà une valeur de record enregistrée
    function isARecordStored() {
        return localStorage.getItem("record");
    }

    //récupère la valeur du record enregistré
    function getRecordStored() {
        return localStorage.getItem("record");
    }

    //compare le nombre de tries avec le record enregistré
    function isNewRecord() {
        return tries < parseInt(localStorage.getItem("record"));
    }

});